//
//  LevelModel.h
//  SuperRabit
//
//  Created by Junaid Mukhtar on 1/24/14.
//  Copyright 2013 Black Tap Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "PBSCMLParser.h"

@interface LevelModel : NSObject {
    
    int                     enemyCount_;
    int                     length_;
    NSString*               levelBackground_;
    NSString*               difficultyLevel_;
    int                     rewardAmount_;
    int                     remainder_;
    int                     waveGap_;
    int                     totalWaves_;
    NSMutableDictionary*    spriterEnemies_;
    NSMutableArray*         spriterMFs_;
    NSArray*                levelEnemies;
    int                     leftEnemies;
    PBSCMLParser*           bossParser;
}

@property(nonatomic, retain)PBSCMLParser*           bossParser;
@property(nonatomic, retain)NSMutableArray*         spriterMFs;
@property(nonatomic, retain)NSMutableDictionary*    spriterEnemies;
@property(nonatomic, retain)NSString*               levelBackground;
@property(nonatomic, retain)NSString*               difficultyLevel;
@property(nonatomic)        int                     enemyCount;
@property(nonatomic)        int                     leftEnemies;
@property(nonatomic)        int                     length;
@property(nonatomic)        int                     rewardAmount;
@property(nonatomic)        int                     waveGap;

- (void)                makeMFNodes;
- (void)                setRemainder;
- (void)                makeBossParserForLevel:(int) level;
- (void)                makeSpriterNodeAndSaveIt;
- (NSArray* )           getEnemiesForThisLevel;
- (NSMutableArray* )    getWave: (int) wave;
@end
