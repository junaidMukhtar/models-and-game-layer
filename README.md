# README #

### Description of the code ###

Files(8):

###GameDataHandler.h
GameDataHandler.m###

This class is responsible for all the parsing and reading static data (Game Data) as well as Game State both from different plist(s).

######
GameLayer.h
GameLayer.m
This is a Singleton class which rests at the back and manages all the navigation between layers and scenes. Every class asks this singleton class for any kind of navigation.

######
LevelModel.h
LevelModel.m
This is the model class of the game level. Each level is different from the other like increased difficulty, different enemies etc. All this metadata is acquired from the GameData object parsed at game start.

######
User.h
User.m
This class (self explanatory) manages the user related stuff like level, coins, xp, purchased coins (through IAP).
