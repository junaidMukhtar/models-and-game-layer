//
//  GameDataHandler.m
//  SuperRabit
//
//  Created by Junaid Mukhtar on 1/16/14.
//  Copyright 2013 Black Tap Studio. All rights reserved.
//

#import "GameDataHandler.h"
#import "JSONKit.h"
#import "Constants.h"
#import "SynthesizeSingleton.h"
#import "User.h"
#import "MegaPowerup.h"
#import "EnemyModel.h"
#import "PlayerModel.h"
#import "LevelModel.h"
#import "GameState.h"
#import "User.h"
#import "AppSettings.h"

@implementation GameDataHandler

@synthesize     levelsData      = levelsData_;
@synthesize     gameData        = gameData_;
@synthesize     gameState       = gameState_;
@synthesize     playerData      = playerData_;
@synthesize     allEnemies      = allEnemies_;
@synthesize     megaPowerUps    = megaPowerUps_;

SYNTHESIZE_SINGLETON_FOR_CLASS(GameDataHandler);

-(id) init {

    if (self = [super init]) {

        [AppSettings sharedInstance];
        
        gameData_ = [AppSettings objectForKey:@"gamemodel"];//[NSDictionary dictionaryWithContentsOfFile:path];
        levelsData_ =[AppSettings objectForKey:@"levelsData"];//[NSArray arrayWithContentsOfFile:levelsDataJsonpath];
        [levelsData_ retain];
        
        NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString * dbPath = [NSString stringWithFormat:@"%@/%@.json",documentsPath,GAME_STATE];
        
        BOOL noError = false;
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:dbPath];
        
        if (!fileExists)
        {
            NSError *error;
            NSString * dbDirectory = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"DB"];
            NSString * path = [NSString stringWithFormat:@"%@/%@.json",dbDirectory,GAME_STATE];
            NSFileManager *fileManager = [NSFileManager defaultManager];
            BOOL copySuccess = [fileManager copyItemAtPath:path toPath:dbPath error:&error];
            if(copySuccess)
            {
                noError = TRUE;
            }
        }
        else
        {
            noError = TRUE;
        }
        
        if(noError)
        {
            NSError *err = nil;
            NSData* jsonData = [NSData dataWithContentsOfFile: dbPath options:NSDataReadingMappedIfSafe error:&err];
            NSLog(@"error: %@", err);
            
            
            JSONDecoder* gameStatedecoder = [[JSONDecoder alloc] initWithParseOptions:JKParseOptionNone];
            gameState_ = [gameStatedecoder objectWithData:jsonData];
            [gameStatedecoder release];
        }
        else
        {
//            [[NSThread mainThread] exit];
        }
    }
    return self;
}

- (void) resetGame{
    
    NSString* shouldReset = [AppSettings objectForKey:@"resetGame"];
    if ([shouldReset isEqualToString:@"1"]) {
        
        NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString * dbPath = [NSString stringWithFormat:@"%@/%@.json",documentsPath,GAME_STATE];
        BOOL noError = false;
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:dbPath];
        
        if (fileExists) {
            
            NSError *error;
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            BOOL deleteSuccess = [fileManager removeItemAtPath:dbPath error:&error];
            if(deleteSuccess)
            {
                noError = TRUE;
            }
        }
        else{
            //already deleted
        }
        if (noError) {
            
            NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString * dbPath = [NSString stringWithFormat:@"%@/%@.json",documentsPath,GAME_STATE];
            
            BOOL noError = false;
            BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:dbPath];
            
            if (!fileExists)
            {
                NSError *error;
                NSString * dbDirectory = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"DB"];
                NSString * path = [NSString stringWithFormat:@"%@/%@.json",dbDirectory,GAME_STATE];
                NSFileManager *fileManager = [NSFileManager defaultManager];
                BOOL copySuccess = [fileManager copyItemAtPath:path toPath:dbPath error:&error];
                if(copySuccess)
                {
                    noError = TRUE;
                }
            }
            else
            {
                noError = TRUE;
            }
            
            if(noError)
            {
                NSError *err = nil;
                NSData* jsonData = [NSData dataWithContentsOfFile: dbPath options:NSDataReadingMappedIfSafe error:&err];
                NSLog(@"error: %@", err);
                
                JSONDecoder* gameStatedecoder = [[JSONDecoder alloc] initWithParseOptions:JKParseOptionNone];
                gameState_ = [gameStatedecoder objectWithData:jsonData];
                //            [gameState_ retain];
                [gameStatedecoder release];
            }
            else
            {
//                [[NSThread mainThread] exit];
            }
        }
    }
    else {
        // reset level, powerups, health
        [[GameState sharedGameState] softReset];
    }
}

-(void) loadGameData {
    
    if (gameData_) {
        
        allEnemies_     = [gameData_ objectForKey:STORE_ENEMEY];
        [allEnemies_ retain];
        playerData_     = [gameData_ objectForKey:STORE_PLAYER];
        [playerData_ retain];
        megaPowerUps_   = [playerData_ objectForKey:STORE_MEGA_POWERUPS];        
    }
}

#pragma mark Getter Functions

-(MegaPowerup*) getMegaPowerUpObjectWithID:(NSString*)p_id andLevel:(NSString*)level {
    
    NSDictionary *mega;
    NSArray *tempArray = [[[GameState sharedGameState] gameUser] megaPowerups];
    
    for (int i=0, length = [tempArray count]; i<length; i++) {
    
        if ([[(NSDictionary*)[tempArray objectAtIndex:i] objectForKey:@"id"] isEqualToString:p_id]) {
            
            mega = [(NSDictionary*)[megaPowerUps_ objectAtIndex:STR2INT(p_id)] objectForKey:level];
            
            MegaPowerup *powerUpObject = [[MegaPowerup alloc] init];
            [powerUpObject setLevel:        STR2INT(level)];
            [powerUpObject setP_id:         p_id];
            [powerUpObject setAbility:              [mega objectForKey:@"ability"]];
            [powerUpObject setAbilityAmount:STR2INT([mega objectForKey:@"abilityAmount"])];
            [powerUpObject setCoolDown:     STR2INT([mega objectForKey:@"coolDown"])];
            [powerUpObject setCost:         STR2INT([mega objectForKey:@"cost"])];
            [powerUpObject setDamage:       STR2INT([mega objectForKey:@"damage"])];
            [powerUpObject setDuration:     STR2INT([mega objectForKey:@"duration"])];
            
            return [powerUpObject autorelease];
        }
    }
    return NULL;
}

-(EnemyModel*)  getEnemyModelForID:(int)e_id {
    
    if (allEnemies_) {
        
        for (int i=0, length = [allEnemies_ count]; i<length; i++) {
            
            if(STR2INT([(NSDictionary*)[allEnemies_ objectAtIndex:i] objectForKey:@"id"]) == e_id) {
                
                EnemyModel *enemyModel = [[EnemyModel alloc] init];
                [enemyModel setRewardCoins:             [(NSDictionary*)[allEnemies_ objectAtIndex:i] objectForKey:@"rewardCoins"]];
                [enemyModel setAttackDamage:    STR2INT([(NSDictionary*)[allEnemies_ objectAtIndex:i] objectForKey:ATTACK_DAMAGE])];
                [enemyModel setAttackSpeed:     STR2FLOAT([(NSDictionary*)[allEnemies_ objectAtIndex:i] objectForKey:ATTACK_SPEED])];
                [enemyModel setEnemyType:               [(NSDictionary*)[allEnemies_ objectAtIndex:i] objectForKey:@"type"]];
                [enemyModel setE_id:            STR2INT([(NSDictionary*)[allEnemies_ objectAtIndex:i] objectForKey:@"id"])];
                [enemyModel setHitPoints:       STR2INT([(NSDictionary*)[allEnemies_ objectAtIndex:i] objectForKey:@"hitPoints"])];
                [enemyModel setMovementSpeed:   STR2INT([(NSDictionary*)[allEnemies_ objectAtIndex:i] objectForKey:@"movementSpeed"])];
                [enemyModel setDifficultyLevel:         [(NSDictionary*)[allEnemies_ objectAtIndex:i] objectForKey:DIFFICULTY_LEVEL]];
                return [enemyModel autorelease];
            }
        }
    }
    return NULL;
}

-(PlayerModel*) getPlayerModel {
    
    if (playerData_) {
        
        PlayerModel *model = [[PlayerModel alloc] init];
        [model setAttackSpeed:STR2FLOAT([playerData_ objectForKey:ATTACK_SPEED])];
        [model setFeetDamage:STR2INT([playerData_ objectForKey:STORE_FEET_DAMAGE])];
        [model setHandsDamage:STR2INT([playerData_ objectForKey:STORE_HANDS_DAMAGE])];
        [model setHealth:STR2INT([playerData_ objectForKey:HEALTH])];
        [model setPlayerPowerUps:[playerData_ objectForKey:STORE_POWERUPS]];
        [model checkPowerUpLevels];
        return model;
    }
    return NULL;
}

//Fetch User Model and make User Object.
-(User *) getUser {
    
    User *user = [[User alloc] init];
    
    NSMutableArray * gameData = (NSMutableArray*)[gameState_ objectForKey:STORE_MEGA_POWERUPS];
    [user temp: gameData];
    [user setArmorUpgradeLevel: STR2INT([gameState_     objectForKey:STORE_ARMOR_LEVEL])];
    [user setCurrentLevel:      STR2INT([gameState_     objectForKey:STORE_LEVEL])];
    [user setCoins:             STR2INT([gameState_     objectForKey:STORE_COINS])];
    [user setPurchasedCoins:    STR2INT([gameState_     objectForKey:STORE_PURCHASED_COINS])];
    [user setFeetUpgradeLevel:  STR2INT([gameState_     objectForKey:STORE_FEET_LEVEL])];
    [user setHandsUpgradeLevel: STR2INT([gameState_     objectForKey:STORE_HANDS_LEVEL])];
    [user setHp:                STR2INT([gameState_     objectForKey:STORE_HP])];
    [user setStrengthLevel:     STR2INT([gameState_     objectForKey:STORE_STRENGTH_LEVEL])];
    [user setXp:                STR2INT([gameState_     objectForKey:STORE_XP])];
    [user setFeetDamage:        STR2INT([gameState_     objectForKey:STORE_FEET_DAMAGE])];
    [user setHandsDamage:       STR2INT([gameState_     objectForKey:STORE_HANDS_DAMAGE])];
    gameState_ = NULL;
    return user;
}

-(LevelModel *)getLevel: (int) levelId {
    
    levelId--;
    if (levelsData_) {
        
        LevelModel * model = [[LevelModel alloc] init];
        [model setDifficultyLevel:          [(NSDictionary*)[levelsData_ objectAtIndex:levelId] objectForKey:@"difficultyLevel"]];
        [model setLevelBackground:          [(NSDictionary*)[levelsData_ objectAtIndex:levelId] objectForKey:@"levelBackground"]];
        [model setEnemyCount:       STR2INT([(NSDictionary*)[levelsData_ objectAtIndex:levelId] objectForKey:@"enemyCount"])];
        [model setLength:           STR2INT([(NSDictionary*)[levelsData_ objectAtIndex:levelId] objectForKey:@"length"])];
        [model setRewardAmount:     STR2INT([(NSDictionary*)[levelsData_ objectAtIndex:levelId] objectForKey:@"rewardAmount"])];
        [model setWaveGap:          STR2INT([(NSDictionary*)[levelsData_ objectAtIndex:levelId] objectForKey:@"waveGap"])];
        [model setRemainder];
        return model;
    }
    return NULL;
}


#pragma mark Update Block

-(void) updateUserGameState:(User*)user {
    
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString * dbPath = [NSString stringWithFormat:@"%@/%@.json",documentsPath,GAME_STATE];
    
    NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];

    [tempDict setObject:        [user megaPowerups]         forKey:STORE_MEGA_POWERUPS];
    [tempDict setObject:INT2STR([user armorUpgradeLevel])   forKey:STORE_ARMOR_LEVEL];
    [tempDict setObject:INT2STR([user currentLevel])        forKey:STORE_LEVEL];
    [tempDict setObject:INT2STR([user coins])               forKey:STORE_COINS];
    [tempDict setObject:INT2STR([user purchasedCoins])      forKey:STORE_PURCHASED_COINS];
    [tempDict setObject:INT2STR([user feetUpgradeLevel])    forKey:STORE_FEET_LEVEL];
    [tempDict setObject:INT2STR([user handsUpgradeLevel])   forKey:STORE_HANDS_LEVEL];
    [tempDict setObject:INT2STR([user hp])                  forKey:STORE_HP];
    [tempDict setObject:INT2STR([user strengthLevel])       forKey:STORE_STRENGTH_LEVEL];
    [tempDict setObject:INT2STR([user feetDamage])          forKey:STORE_FEET_DAMAGE];
    [tempDict setObject:INT2STR([user handsDamage])         forKey:STORE_HANDS_DAMAGE];
    [tempDict setObject:INT2STR([user xp])                  forKey:STORE_XP];
    gameState_ = tempDict;
    
    NSOutputStream *os = [[NSOutputStream alloc] initToFileAtPath:dbPath append:NO];
    [os open];
    [NSJSONSerialization writeJSONObject:tempDict toStream:os options:0 error:nil];
    [tempDict release];
    [os close];
}

#pragma mark GameState Save Block


-(void) dealloc {
    
    [levelsData_ release];
    [playerData_ release];
    [allEnemies_ release];
    [super dealloc];
}
@end