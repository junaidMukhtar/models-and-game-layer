//
//  GameLayer.m
//  SuperRabit
//
//  Created by Junaid Mukhtar on 1/20/14.
//  Copyright 2013 Black Tap Studio. All rights reserved.
//

#import "GameLayer.h"
#import "MainMenuUI.h"
#import "SynthesizeSingleton.h"
#import "TopPanel.h"
#import "LevelLayer.h"
#import "GameState.h"
#import "UpgradePanel.h"
#import "OptionsUI.h"
#import "Constants.h"
#import "GameUtils.h"
#import "TutorialUI.h"
#import "IntroLayer.h"
#import "User.h"
//#import "iRate.h"
#import "AppDelegate.h"
#import "GameLoader.h"

@implementation GameLayer

SYNTHESIZE_SINGLETON_FOR_CLASS(GameLayer);

@synthesize rewardCoins;
@synthesize isGamePaused;
@synthesize isGameLaunch = isGameLaunch_;
@synthesize levelSucceeded = levelSucceeded_;
@synthesize cameFromLevel = cameFromLevel_;
@synthesize levelLayerObject = levelLayerObject_;

+(CCScene *) scene {
    
	CCScene *scene = [CCScene node];
	GameLayer *layer = [GameLayer node];
	[scene addChild: layer];
	return scene;
}

-(void)onEnter {
    
    if(isGameLaunch_) {
        
        isGameLaunch_ = NO;
        [super onEnter];
        bool isNotFirstRun = [[NSUserDefaults standardUserDefaults] boolForKey:SR_NOT_FIRST_RUN];
        if (!isNotFirstRun) {
            
            [GameUtils enableEffects];
            [GameUtils enableMusic];
            [[NSUserDefaults standardUserDefaults] setBool:true forKey:SR_NOT_FIRST_RUN];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        [self initialize];
    }
}

-(id) init {
	if( (self=[super init]) ) {
        
        isGameLaunch_ = YES;
	}
	return self;
}

- (void) initialize {
    
    [GameUtils playBackgroundMusic];
    [self showMainMenuUI];
    [self checkAndShowRating];
}

- (void) checkAndShowRating {

//    [[AdsManager sharedAdsManager] cancelAdsForCurrentLocation];
//    AppController * controller = (AppController *) [[UIApplication sharedApplication] delegate];
//    [controller showRateController];
//    [[iRate sharedInstance] tryPromptForRating];
}

- (void) showMainMenuUI {
   
//    ccColor3B color = ccBLACK;
    [[CCDirector sharedDirector] pushScene:[CCTransitionSlideInL transitionWithDuration:0.1 scene:[MainMenuUI scene]]];
}

-(void) showUpgradePanel {
    
    if (![[GameLoader sharedGameLoader] gameStarted]) {
        
        [[GameLoader sharedGameLoader] parseGameData];
        [[GameLoader sharedGameLoader] setGameStarted:YES];
    }
    CCScene* scene  =[UpgradePanel scene];
    upgradeLayer = (UpgradePanel* )[scene getChildByTag:UPGRADE_LAYER];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionSlideInR transitionWithDuration:0.1f scene:scene]];
}

-(void) updateCoinsAfterRating {
    
    if (upgradeLayer) {
        [[upgradeLayer coinsLabel] setString:INT2STR([[[GameState sharedGameState] gameUser] coins])];
    }
}

-(void) showOptionsUI {
    
//    ccColor3B color = ccBLACK;
    [[CCDirector sharedDirector] replaceScene:[CCTransitionSlideInR transitionWithDuration:0.1 scene:[OptionsUI optionsScene]]];
}

-(void) showHelpUI {
    
//    ccColor3B color = ccBLACK;
    [[CCDirector sharedDirector] replaceScene:[CCTransitionSlideInR transitionWithDuration:0.1 scene:[TutorialUI scene]]];
}

-(void) initiateLevel {
    
    if (![[GameLoader sharedGameLoader] gameStarted]) {
        
        [[GameLoader sharedGameLoader] parseGameData];
        [[GameLoader sharedGameLoader] setGameStarted:YES];
    }
    if (levelSucceeded_) {
        
        [[CCDirector sharedDirector] popScene];
        levelSucceeded_ = false;
    }
    
    ccColor3B color = ccBLACK;
    
    levelLayerObject_ = [[LevelLayer alloc] init];;
    CCScene* scene = [CCScene node];
    [scene addChild:levelLayerObject_];
    
    [[CCDirector sharedDirector] pushScene:[CCTransitionFade transitionWithDuration:1 scene:scene withColor:color]];
}

-(void) endIntro {
    
    NSString * pathToAdsLocationsPlist = [[NSBundle mainBundle] pathForResource:@"Utility" ofType:@"plist"];
    NSDictionary* plistDict = [NSDictionary dictionaryWithContentsOfFile:pathToAdsLocationsPlist];
    
    if (![[plistDict objectForKey:@"Application_Name"] isEqualToString:@"Horse Fighter"]) {
        
        [[CCDirector sharedDirector] popScene];
    }
    [self initiateLevel];
}

-(void) showIntro {
    
    ccColor3B color = ccBLACK;
    CCScene* scene = [IntroLayer scene];
    [[CCDirector sharedDirector] pushScene:[CCTransitionFade transitionWithDuration:1 scene:scene withColor:color]];    
}

- (void) backToMainMenu {
    
    [[CCDirector sharedDirector] popScene];
    [self showMainMenuUI];
}

- (void) levelEnded {
    
    cameFromLevel_ = true;
    [[GameState sharedGameState] updateUser];
    [self showUpgradePanel];
}

- (void) levelSuccessWithOption {
    
    levelSucceeded_ = true;
    cameFromLevel_ = true;    
    
    int currentLevel = [[[GameState sharedGameState] gameUser] currentLevel];
    
    if (currentLevel == 5 || currentLevel == 9 || currentLevel == 13) {
        
        [[CCDirector sharedDirector] popScene];
        
        NSString * pathToAdsLocationsPlist = [[NSBundle mainBundle] pathForResource:@"Utility" ofType:@"plist"];
        NSDictionary* plistDict = [NSDictionary dictionaryWithContentsOfFile:pathToAdsLocationsPlist];
        
        if (![[plistDict objectForKey:@"Application_Name"] isEqualToString:@"Horse Fighter"] || [[plistDict objectForKey:@"Application_Name"] isEqualToString:@"Easter Games 2014 - Bunny Egg Treasure Hunt"]) {
            
            [[GameLayer sharedGameLayer] showIntro];
        }
        else {
            [[GameLayer sharedGameLayer] showUpgradePanel];
        }
    }
    else {
//        bool rated = [[NSUserDefaults standardUserDefaults] boolForKey:@"RATING_DONE"];
//        if (!rated && (currentLevel == 3 || currentLevel == 5 || currentLevel == 11)) { //after completing 2 levels, prompt for rating.
//            
//            rewardCoins = true;
//            [[AdsManager sharedAdsManager] cancelAdsForCurrentLocation];
//            AppController * controller = (AppController *) [[UIApplication sharedApplication] delegate];
//            [controller showRateController];
//        }
        [self showUpgradePanel];
    }
}

- (void) retryLevel {
    
    [[CCDirector sharedDirector] popScene];
    [self initiateLevel];
}

- (void) dealloc {
    
	[super dealloc];
}
@end
