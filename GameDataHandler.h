//
//  GameDataHandler.h
//  SuperRabit
//
//  Created by Junaid Mukhtar on 1/16/14.
//  Copyright 2013 Black Tap Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "AppSettings.h"

@class User, MegaPowerup, EnemyModel, PlayerModel, LevelModel;

@interface GameDataHandler : NSObject<AppSettingsDelegate> {
    NSDictionary*   gameData_;
    NSArray*        allEnemies_;
    NSArray*        megaPowerUps_;
    NSDictionary*   playerData_;
    NSDictionary*   gameState_;
    NSArray*        levelsData_;
}

@property(nonatomic, retain) NSArray*       levelsData;
@property(nonatomic, retain) NSDictionary*  gameData;
@property(nonatomic, retain) NSDictionary*  gameState;
@property(nonatomic, retain) NSDictionary*  playerData;
@property(nonatomic, retain) NSArray*       allEnemies;
@property(nonatomic, retain) NSArray*       megaPowerUps;

+(GameDataHandler*) sharedGameDataHandler;
-(void)             loadGameData;
-(User*)            getUser;
-(void)             resetGame;
-(void)             updateUserGameState:(User*)user;
-(MegaPowerup*)     getMegaPowerUpObjectWithID:(NSString*)p_id andLevel:(NSString*)level;
-(EnemyModel*)      getEnemyModelForID:(int)e_id;
-(PlayerModel*)     getPlayerModel;
-(LevelModel *)     getLevel:(int) levelId ;

@end
