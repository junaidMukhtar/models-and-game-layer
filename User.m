//
//  User.m
//  SuperRabit
//
//  Created by Junaid Mukhtar on 1/17/14.
//  Copyright 2013 Black Tap Studio. All rights reserved.
//

#import "User.h"
#import "Constants.h"

@implementation User
@synthesize hp                  = hp_;
@synthesize coins               = coins_;
@synthesize xp                  = xp_;
@synthesize currentLevel        = currentLevel_;
@synthesize megaPowerups        = megaPowerups_;
@synthesize strengthLevel       = strengthLevel_;
@synthesize feetUpgradeLevel    = feetUpgradeLevel_;
@synthesize handsUpgradeLevel   = handsUpgradeLevel_;
@synthesize armorUpgradeLevel   = armorUpgradeLevel_;
@synthesize purchasedCoins       = purchasedCoins_;
@synthesize feetDamage          = feetDamage_;
@synthesize handsDamage         = handsDamage_;

-(id) init {
    if (self = [super init]) {
        
        megaPowerups_ = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void) temp: (NSArray *) test
{
//    megaPowerups_ =
    for (int i=0; i<[test count]; i++) {
        NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
        [dict setObject:[[test objectAtIndex:i] objectForKey:@"id"] forKey:@"id"];
        [dict setObject:[[test objectAtIndex:i] objectForKey:@"level"] forKey:@"level"];
        [dict setObject:[[test objectAtIndex:i] objectForKey:@"equipped"] forKey:@"equipped"];
        [megaPowerups_ addObject:dict];
        [dict release];
    }
//    megaPowerups_ = (NSMutableArray*)[NSMutableArray arrayWithArray:[test objectForKey:STORE_MEGA_POWERUPS]];
    
//    megaPowerups_ = (NSMutableArray*) [NSMutableArray arrayWithArray:arra];
    [megaPowerups_ retain];
}

- (void)dealloc
{    
    [super dealloc];
}
@end