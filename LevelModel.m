//
//  LevelModel.m
//  SuperRabit
//
//  Created by Junaid Mukhtar on 1/24/14.
//  Copyright 2013 Black Tap Studio. All rights reserved.
//

#import "LevelModel.h"
#import "LevelLayer.h"
#import "GameDataHandler.h"
#import "PBSpriterNode.h"
#import "Constants.h"
#import "PBSCMLParser.h"
#import "GameState.h"
#import "MegaPowerup.h"

@implementation LevelModel
@synthesize leftEnemies;
@synthesize bossParser;
@synthesize spriterMFs      = spriterMFs_;
@synthesize spriterEnemies  = spriterEnemies_;
@synthesize levelBackground = levelBackground_;
@synthesize difficultyLevel = difficultyLevel_;
@synthesize enemyCount      = enemyCount_;
@synthesize length          = length_;
@synthesize rewardAmount    = rewardAmount_;
@synthesize waveGap         = waveGap_;

-(id) init {
    
    if (self = [super init]) {

        spriterEnemies_ = [[NSMutableDictionary alloc] init];
        spriterMFs_ = [[NSMutableArray alloc] init];
        [self makeMFNodes];
    }
    return self;
}

-(void) setRemainder {
    
    leftEnemies = enemyCount_;
    totalWaves_ = length_/waveGap_;
    remainder_ = enemyCount_%totalWaves_;
}

- (NSMutableArray* ) getWave: (int) wave {
    
//    levelEnemies = [self getEnemiesForThisLevel];
//    [levelEnemies retain];
    NSMutableArray *waveEnemies = [[NSMutableArray alloc] init];
    
    int num = enemyCount_/totalWaves_;
    
    if (remainder_ >0) {
        
        int shouldAdd = arc4random()%2;
        
        if (shouldAdd == 1) {
            
//            NSLog(@"junaid adding extra enemy to the wave");
            num += 1;
            remainder_ --;
        }
        else if (wave == totalWaves_) {
            
//            NSLog(@"junaid adding enemies to the last wave: %i",remainder_);
            num+= remainder_;
            remainder_ = 0;
        }
    }
    leftEnemies -= num;
//    NSLog(@"making enemies count: %i", num);
    
    for (int i =0, l = num; i <l; i++) {
        int randomIndex = arc4random() % [levelEnemies count];
        if (randomIndex == [levelEnemies count]) {
            randomIndex -=1;
        }
        [waveEnemies addObject:[levelEnemies objectAtIndex:randomIndex]];
    }
    return [waveEnemies autorelease];
}

- (void) makeSpriterNodeAndSaveIt {
    
    levelEnemies = [self getEnemiesForThisLevel];
    [levelEnemies retain];
    NSMutableArray * enemyIds = [[NSMutableArray alloc] init];
    BOOL shouldAdd = true;
    
    for (int i=0; i<[levelEnemies count]; i++) {
        
        int key = STR2INT([(NSDictionary*)[levelEnemies objectAtIndex:i] objectForKey:@"id"]);

        if ([enemyIds count] == 0) {
            
            [enemyIds addObject:INT2STR(key)];
        }
        
        for (int j=0; j<[enemyIds count]; j++) {
            
            int enemyId = STR2INT([enemyIds objectAtIndex:j]);
            if (key == enemyId) {
                
                shouldAdd = false;
            }
        }
        
        if (shouldAdd) {
            
//            NSLog(@"adding key");
            [enemyIds addObject:INT2STR(key)];
        }
        shouldAdd = true;
    }
    NSString * enemyDirectory = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:ENEMY_DIRECTORY];
    
    for (int i=0; i<[enemyIds count]; i++) {
        
        int enemyId_ = STR2INT([enemyIds objectAtIndex:i]);
        
        NSString * scmlPath = [NSString stringWithFormat:@"%@/%i/%i.scml",enemyDirectory,enemyId_,enemyId_];
        NSString * plistPath = [NSString stringWithFormat:@"%@/%i/%i.plist",enemyDirectory,enemyId_,enemyId_];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:plistPath];
        
        PBSCMLParser *parser = [[PBSCMLParser alloc] initNodeWithFiles:scmlPath];
        [spriterEnemies_ setObject:parser forKey:INT2STR(enemyId_)];
        [parser release];
    }
    
    [enemyIds release];
}

-(void) makeBossParserForLevel:(int) level {
    
    int bossId = 11;
    if (level == 8) {
        bossId = 12;
    }else if(level == 12){
        bossId = 13;
    }
    
    NSString * enemyDirectory = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:ENEMY_DIRECTORY];
    NSString * scmlPath = [NSString stringWithFormat:@"%@/%i/%i.scml",enemyDirectory,bossId,bossId];
    
    NSString * plistPath = [NSString stringWithFormat:@"%@/%i/%i.plist",enemyDirectory,bossId,bossId];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:plistPath];
    
    PBSCMLParser *parser = [[PBSCMLParser alloc] initNodeWithFiles:scmlPath];
    bossParser = parser;
}

-(void) makeMFNodes {
    
    NSArray *abilities = [[GameState sharedGameState] megaPowers];
    int count = 0;
    if ([abilities count] >0) {
        for (int i=0; i < [abilities count]; i++) {
            
            if ([[[abilities objectAtIndex:i] p_id] isEqualToString:INT2STR(mini)]) {
//                [arr addObject:[abilities objectAtIndex:i]];
                count = [(MegaPowerup*)[abilities objectAtIndex:i] level];
            }
        }
    }

    for (int i=0; i< count; i++) {
        PBSCMLParser *parser = [[PBSCMLParser alloc] initNodeWithFiles:@"rabbit.scml"];
        [spriterMFs_ addObject:parser];
        [parser release];
    }
}

-(NSArray*) getEnemiesForThisLevel {
    
    NSArray* enemies = [[GameDataHandler sharedGameDataHandler] allEnemies];
    NSMutableArray* tempArray = [[NSMutableArray alloc] initWithCapacity:enemyCount_];
    NSMutableArray* levelDependentEnemies = [[NSMutableArray alloc] init];
    int random;
    
    if ([difficultyLevel_ isEqualToString:@"easy"]) {
        
        for (int i=0, l= [enemies count]; i<l; i++ ) {
            if ([[(NSDictionary*)[enemies objectAtIndex:i] objectForKey:@"difficultyLevel"] isEqualToString:@"easy"]) {
                [levelDependentEnemies addObject:[enemies objectAtIndex:i]];
            }
        }
    }
    
    else if([difficultyLevel_ isEqualToString:@"medium"]) {
        
//        for (int i=0, l= [enemies count]; i<l; i++ ) {
//            if ([[(NSDictionary*)[enemies objectAtIndex:i] objectForKey:@"difficultyLevel"] isEqualToString:@"easy"]) {
//                [levelDependentEnemies addObject:[enemies objectAtIndex:i]];
//            }
//        }
        for (int i=0, l= [enemies count]; i<l; i++ ) {
            if ([[(NSDictionary*)[enemies objectAtIndex:i] objectForKey:@"difficultyLevel"] isEqualToString:@"medium"]) {
                [levelDependentEnemies addObject:[enemies objectAtIndex:i]];
            }
        }
    }
    
    else if([difficultyLevel_ isEqualToString:@"hard"]) {
                
//        for (int i=0, l= [enemies count]; i<l; i++ ) {
//
//            if ([[(NSDictionary*)[enemies objectAtIndex:i] objectForKey:@"difficultyLevel"] isEqualToString:@"easy"]) {
//                [levelDependentEnemies addObject:[enemies objectAtIndex:i]];
//            }
//        }
//        for (int i=0, l= [enemies count]; i<l; i++ ) {
//            
//            if ([[(NSDictionary*)[enemies objectAtIndex:i] objectForKey:@"difficultyLevel"] isEqualToString:@"medium"]) {
//                [levelDependentEnemies addObject:[enemies objectAtIndex:i]];
//            }
//        }
        
        for (int i=0, l= [enemies count]; i<l; i++ ) {
            if ([[(NSDictionary*)[enemies objectAtIndex:i] objectForKey:@"difficultyLevel"] isEqualToString:@"hard"]) {
                [levelDependentEnemies addObject:[enemies objectAtIndex:i]];
            }
        }
    }
    
    else if([difficultyLevel_ isEqualToString:@"insane"]) {
        
//        for (int i=0, l= [enemies count]; i<l; i++ ) {
//            if ([[(NSDictionary*)[enemies objectAtIndex:i] objectForKey:@"difficultyLevel"] isEqualToString:@"hard"]) {
//                [levelDependentEnemies addObject:[enemies objectAtIndex:i]];
//            }
//        }
        
        for (int i=0, l= [enemies count]; i<l; i++ ) {
            if ([[(NSDictionary*)[enemies objectAtIndex:i] objectForKey:@"difficultyLevel"] isEqualToString:@"insane"]) {
                [levelDependentEnemies addObject:[enemies objectAtIndex:i]];
            }
        }
    }

    for (int j=0; j<enemyCount_; j++) {
        random = arc4random()%([levelDependentEnemies count]);
        [tempArray addObject:[levelDependentEnemies objectAtIndex:random]];
    }
    [levelDependentEnemies autorelease];
    return [tempArray autorelease];
}

- (void)dealloc
{    
    [super dealloc];
}
@end