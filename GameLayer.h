//
//  GameLayer.h
//  SuperRabit
//
//  Created by Junaid Mukhtar on 1/20/14.
//  Copyright 2013 Black Tap Studio. All rights reserved.
//

#import <GameKit/GameKit.h>
#import "cocos2d.h"

@class UpgradePanel, LevelLayer;

@interface GameLayer : CCLayer
{
    BOOL            levelSucceeded_;
    BOOL            isGamePaused_;
    BOOL            cameFromLevel_;
    BOOL            rewardCoins;
    UpgradePanel*   upgradeLayer;
    LevelLayer*     levelLayerObject_;
}

@property(nonatomic, retain) LevelLayer* levelLayerObject;
@property(nonatomic)BOOL rewardCoins;
@property(nonatomic)BOOL cameFromLevel;
@property(nonatomic)BOOL isGamePaused;
@property(nonatomic)BOOL levelSucceeded;
@property(nonatomic)BOOL isGameLaunch;

+ (GameLayer*)  sharedGameLayer;
+ (CCScene*)    scene;
- (void)        initialize;
- (void)        showMainMenuUI;
- (void)        initiateLevel;
- (void)        levelEnded;
- (void)        backToMainMenu;
- (void)        showUpgradePanel;
- (void)        showOptionsUI;
- (void)        showHelpUI;
- (void)        retryLevel;
- (void)        showIntro;
- (void)        endIntro;
- (void)        levelSuccessWithOption;
- (void)        updateCoinsAfterRating;
@end
