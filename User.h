//
//  User.h
//  SuperRabit
//
//  Created by Junaid Mukhtar on 1/17/14.
//  Copyright 2013 Black Tap Studio. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface User : NSObject {
    
    int             coins_;
    int             purchasedCoins_;
    int             xp_;
    int             hp_;
    int             currentLevel_;
    int             strengthLevel_;
    int             handsUpgradeLevel_;
    int             feetUpgradeLevel_;
    int             armorUpgradeLevel_;
    int             feetDamage_;
    int             handsDamage_;
    NSMutableArray* megaPowerups_;
}

@property(nonatomic)        int             xp;
@property(nonatomic)        int             hp;
@property(nonatomic)        int             coins;
@property(nonatomic)        int             purchasedCoins;
@property(nonatomic)        int             currentLevel;
@property(nonatomic)        int             strengthLevel;
@property(nonatomic)        int             handsUpgradeLevel;
@property(nonatomic)        int             feetUpgradeLevel;
@property(nonatomic)        int             armorUpgradeLevel;
@property(nonatomic)        int             handsDamage;
@property(nonatomic)        int             feetDamage;
@property(nonatomic, retain)NSMutableArray* megaPowerups;

- (void) temp: (NSArray *) test;
@end
